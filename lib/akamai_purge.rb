require 'akamai_purge/version'

module AkamaiPurge
  class Client
    AKAMAI_API            = 'https://api.ccu.akamai.com'
    AKAMAI_API_QUEUE      = '/ccu/v2/queues/default'
    AKAMAI_API_PURGES	    = '/ccu/v2/purges/'
    AKAMAI_TIMEOUT        = 10
    AKAMAI_OPEN_TIMEOUT   = 10
    AKAMAI_DOMAIN         = 'production'


    @akamai_username
    @akamai_password

    @akamai_timeout
    @akamai_open_timeout
    @akamai_domain

    def initialize(akamai_username, akamai_password, options = {})
      @akamai_username = akamai_username
      @akamai_password = akamai_password

      @akamai_timeout = options[:akamai_timeout] || AKAMAI_TIMEOUT
      @akamai_open_timeout = options[:akamai_open_timeout] || AKAMAI_OPEN_TIMEOUT
      @akamai_domain = options[:akamai_domain] || AKAMAI_DOMAIN
    end

    def purge(cp_code)
      akamai_api 	= "#{AKAMAI_API}#{AKAMAI_API_QUEUE}"
      payload_cpcode  = cp_code.split(',').flatten

      payload = {
          :type       => 'cpcode',
          :objects    => payload_cpcode,
          :domain     => @akamai_domain,
          :action     => 'remove'
      }

      begin
        response = RestClient::Request.new(
            :method         => :post,
            :url            => akamai_api,
            :user           => @akamai_username,
            :password       => @akamai_password,
            :headers        => {
                :accept => :json,
                :content_type => :json
            },
            :timeout        => @akamai_timeout,
            :open_timeout   => @akamai_open_timeout,
            :payload        => payload.to_json
        ).execute

        json = JSON.parse(response.to_str)


      rescue Exception => e
        raise e
      end

      json
    end

    def status(purge_id)
      akamai_api = "#{AKAMAI_API}#{AKAMAI_API_PURGES}/#{purge_id}"

      begin
        response = RestClient::Request.new(
            :method         => :get,
            :url            => akamai_api,
            :user           => @akamai_username,
            :password       => @akamai_password,
            :headers        => {
                :accept => :json,
                :content_type => :json
            },
            :timeout        => @akamai_timeout,
            :open_timeout   => @akamai_open_timeout
        ).execute

        json = JSON.parse(response.to_str)


      rescue Exception => e
        raise e
      end

      json
    end
  end
end